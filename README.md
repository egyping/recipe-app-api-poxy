# Recipe App API Nginx proxy

nginx proxy app for the recipe project to serve the static files better 

## usage

## Environment Variables

* 'LISTEN_PORT' - Nging will listen to (default: '8000')
* 'APP_HOST' - the app name to forward (default: 'app')
* 'APP_PORT' - Nginx will forward to (default: '9000')

